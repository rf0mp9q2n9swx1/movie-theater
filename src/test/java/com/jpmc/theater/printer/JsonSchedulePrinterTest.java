package com.jpmc.theater.printer;

import static com.jpmc.theater.TestUtils.mockClock;
import static com.jpmc.theater.TestUtils.mockMovie;
import static java.time.Duration.ofMinutes;

import com.jpmc.theater.Schedule;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

class JsonSchedulePrinterTest {
  @Test
  void jsonScheduleShouldBePrintedAsExpected() throws JSONException {
    // given
    var clock = mockClock(2022, 7, 5, 12, 55, 0, 0);
    var localDate = LocalDate.now(clock);
    var localTime = LocalTime.of(12, 34);
    var movie = mockMovie("A movie title", new BigDecimal("1.23"), ofMinutes(61));

    var schedule = new Schedule(clock);
    schedule.addShowing(1, movie, localDate, localTime);

    var printer = new JsonSchedulePrinter();
    var outputStream = new ByteArrayOutputStream();

    // when
    printer.print(outputStream, schedule);

    // then
    var expectedJson = buildExpectedJsonObject().toString();
    var actualJson = outputStream.toString();
    JSONAssert.assertEquals(expectedJson, actualJson, true);
  }

  private JSONObject buildExpectedJsonObject() throws JSONException {
    var showing = new JSONObject();
    showing.put("sequenceOfTheDay", 1);
    showing.put("startTime", "2022-07-05T12:34");
    showing.put("title", "A movie title");
    showing.put("runningTime", "(1 hour 1 minute)");
    showing.put("movieFee", "$1.23");

    var showings = new JSONArray();
    showings.put(0, showing);

    var schedule = new JSONObject();
    schedule.put("date", "2022-07-05");
    schedule.put("showings", showings);

    return schedule;
  }
}
