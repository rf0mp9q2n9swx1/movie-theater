package com.jpmc.theater.printer;

import static com.jpmc.theater.TestUtils.mockClock;
import static com.jpmc.theater.TestUtils.mockMovie;
import static java.time.Duration.ofMinutes;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.jpmc.theater.Schedule;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import org.junit.jupiter.api.Test;

class PlainTextSchedulePrinterTest {
  @Test
  void plainTextScheduleShouldBePrintedAsExpected() {
    // given
    var clock = mockClock(2022, 7, 5, 12, 55, 0, 0);
    var localDate = LocalDate.now(clock);
    var localTime = LocalTime.of(12, 34);
    var movie = mockMovie("A movie title", new BigDecimal("1.23"), ofMinutes(61));

    var schedule = new Schedule(clock);
    schedule.addShowing(1, movie, localDate, localTime);

    var printer = new PlainTextSchedulePrinter();
    var outputStream = new ByteArrayOutputStream();

    // when
    printer.print(outputStream, schedule);

    // then
    var lines = outputStream.toString().split("\n");
    assertEquals(4, lines.length);
    assertEquals("2022-07-05", lines[0]);
    assertEquals("===================================================", lines[1]);
    assertEquals("1: 2022-07-05T12:34 A movie title (1 hour 1 minute) $1.23", lines[2]);
    assertEquals("===================================================", lines[3]);
  }
}
