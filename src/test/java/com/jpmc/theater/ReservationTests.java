package com.jpmc.theater;

import static com.jpmc.theater.TestUtils.mockMovie;
import static com.jpmc.theater.TestUtils.mockShowing;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.jpmc.theater.discount.TicketDiscount;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

class ReservationTests {

  @Test
  void reservationTotalShouldBeCalculatedAsExpected() {
    // given
    var customer = new Customer("John Doe", "john-does-id");
    var ticketPrice = new BigDecimal("10.00");
    var movie = mockMovie(ticketPrice);
    var showing = mockShowing(movie);
    var ticketDiscount = new TicketDiscount(BigDecimal.valueOf(1), BigDecimal.valueOf(10));
    var audienceCount = 3;

    // when
    var reservation = new Reservation(customer, showing, ticketDiscount, audienceCount);

    // then
    assertEquals(customer, reservation.getCustomer());
    assertEquals(showing, reservation.getShowing());
    assertEquals(ticketPrice, reservation.getTicketPrice());
    assertEquals(ticketDiscount, reservation.getTicketDiscount());
    assertEquals(audienceCount, reservation.getAudienceCount());
    assertEquals(new BigDecimal("30.00"), reservation.getTicketPriceTotal());
    assertEquals(new BigDecimal("3.00"), reservation.getTicketDiscountTotal().getDiscountAmount());
    assertEquals(
        new BigDecimal("10.00"), reservation.getTicketDiscountTotal().getDiscountPercentage());
    assertEquals(new BigDecimal("27.00"), reservation.getTotal());
  }
}
