package com.jpmc.theater.discount;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.jpmc.theater.TestUtils;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

class FirstShowingTicketDiscountRuleTest {
  @Test
  void sequenceOfTheDayMatchedShowingDiscountShouldBeCalculatedAsExpected() {
    // given
    var ticketPrice = new BigDecimal("15");
    var movie = TestUtils.mockMovie(ticketPrice);
    var showing = TestUtils.mockShowing(movie, 1);

    // when
    var ticketDiscount = new FirstShowingTicketDiscountRule().calculateDiscount(showing);

    // then
    assertNotNull(ticketDiscount);
    assertEquals(new BigDecimal("20.00"), ticketDiscount.getDiscountPercentage());
    assertEquals(new BigDecimal("3.00"), ticketDiscount.getDiscountAmount());
  }

  @Test
  void negativeTicketPriceDiscountShouldBeCalculatedAsExpected() {
    // given
    var ticketPrice = new BigDecimal("-0.01");
    var movie = TestUtils.mockMovie(ticketPrice);
    var showing = TestUtils.mockShowing(movie, 1);

    // when
    var ticketDiscount = new FirstShowingTicketDiscountRule().calculateDiscount(showing);

    // then
    assertNotNull(ticketDiscount);
    assertEquals(TicketDiscount.NO_DISCOUNT, ticketDiscount);
  }

  @Test
  void ticketPriceLessThanDiscountShouldBeCalculatedAsExpected() {
    // given
    var ticketPrice = new BigDecimal("1");
    var movie = TestUtils.mockMovie(ticketPrice);
    var showing = TestUtils.mockShowing(movie, 1);

    // when
    var ticketDiscount = new FirstShowingTicketDiscountRule().calculateDiscount(showing);

    // then
    assertNotNull(ticketDiscount);
    assertEquals(new BigDecimal("100.00"), ticketDiscount.getDiscountPercentage());
    assertEquals(new BigDecimal("1.00"), ticketDiscount.getDiscountAmount());
  }

  @Test
  void notMatchingSequenceOfTheDayDiscountShouldBeCalculatedAsExpected() {
    // given
    var ticketPrice = new BigDecimal("15");
    var movie = TestUtils.mockMovie(ticketPrice);
    var showing = TestUtils.mockShowing(movie, 2);

    // when
    var ticketDiscount = new FirstShowingTicketDiscountRule().calculateDiscount(showing);

    // then
    assertNotNull(ticketDiscount);
    assertEquals(TicketDiscount.NO_DISCOUNT, ticketDiscount);
  }
}
