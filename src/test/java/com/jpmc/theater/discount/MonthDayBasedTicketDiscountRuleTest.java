package com.jpmc.theater.discount;

import static com.jpmc.theater.TestUtils.mockShowing;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.jpmc.theater.TestUtils;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;

class MonthDayBasedTicketDiscountRuleTest {
  @Test
  void discountAmountForShowingBeforeDiscountDayShouldBeCalculatedAsExpected() {
    // given
    var showingDateTime = LocalDateTime.of(2022, 7, 6, 10, 59, 59, 1);
    var ticketPrice = new BigDecimal("10");
    var movie = TestUtils.mockMovie(ticketPrice);
    var showing = mockShowing(movie, showingDateTime);

    // when
    var ticketDiscount = new MonthDayBasedTicketDiscountRule().calculateDiscount(showing);

    // then
    assertNotNull(ticketDiscount);
    assertEquals(new BigDecimal("0.00"), ticketDiscount.getDiscountPercentage());
    assertEquals(new BigDecimal("0.00"), ticketDiscount.getDiscountAmount());
  }

  @Test
  void discountAmountForShowingDuringDiscountDayShouldBeCalculatedAsExpected() {
    // given
    var showingDateTime = LocalDateTime.of(2022, 7, 7, 0, 0, 0, 1);
    var ticketPrice = new BigDecimal("10");
    var movie = TestUtils.mockMovie(ticketPrice);
    var showing = mockShowing(movie, showingDateTime);

    // when
    var ticketDiscount = new MonthDayBasedTicketDiscountRule().calculateDiscount(showing);

    // then
    assertNotNull(ticketDiscount);
    assertEquals(new BigDecimal("10.00"), ticketDiscount.getDiscountPercentage());
    assertEquals(new BigDecimal("1.00"), ticketDiscount.getDiscountAmount());
  }

  @Test
  void discountAmountForShowingAfterDiscountDayShouldBeCalculatedAsExpected() {
    // given
    var showingDateTime = LocalDateTime.of(2022, 7, 8, 0, 0, 0, 1);
    var ticketPrice = new BigDecimal("10");
    var movie = TestUtils.mockMovie(ticketPrice);
    var showing = mockShowing(movie, showingDateTime);

    // when
    var ticketDiscount = new MonthDayBasedTicketDiscountRule().calculateDiscount(showing);

    // then
    assertNotNull(ticketDiscount);
    assertEquals(new BigDecimal("0.00"), ticketDiscount.getDiscountPercentage());
    assertEquals(new BigDecimal("0.00"), ticketDiscount.getDiscountAmount());
  }
}
