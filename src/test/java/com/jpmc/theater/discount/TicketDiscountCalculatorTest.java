package com.jpmc.theater.discount;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.jpmc.theater.TestUtils;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;

class TicketDiscountCalculatorTest {
  @Test
  void showingWithAllAvailableDiscountRulesShouldPickWithBiggestAmountOne() {
    // given
    var ticketPrice = new BigDecimal(100);
    var showingStartDateTime = LocalDateTime.of(2022, 7, 7, 12, 0, 0, 0);
    var movie = TestUtils.mockMovie(ticketPrice, true);
    var showing = TestUtils.mockShowing(movie, 1, showingStartDateTime);

    // when
    var ticketDiscount = TicketDiscountCalculator.INSTANCE.apply(showing);

    // then
    assertNotNull(ticketDiscount);
    assertEquals(new BigDecimal("25.00"), ticketDiscount.getDiscountPercentage());
    assertEquals(new BigDecimal("25.00"), ticketDiscount.getDiscountAmount());
  }

  @Test
  void showingWithNoMatchingDiscountRulesShouldPickNoDiscount() {
    // given
    var ticketPrice = new BigDecimal(100);
    var showingStartDateTime = LocalDateTime.of(2022, 7, 1, 19, 0, 0, 0);
    var movie = TestUtils.mockMovie(ticketPrice, false);
    var showing = TestUtils.mockShowing(movie, 3, showingStartDateTime);

    // when
    var ticketDiscount = TicketDiscountCalculator.INSTANCE.apply(showing);

    // then
    assertNotNull(ticketDiscount);
    assertEquals(new BigDecimal("0.00"), ticketDiscount.getDiscountPercentage());
    assertEquals(new BigDecimal("0.00"), ticketDiscount.getDiscountAmount());
  }
}
