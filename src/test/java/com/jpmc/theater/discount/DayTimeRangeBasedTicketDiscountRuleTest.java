package com.jpmc.theater.discount;

import static com.jpmc.theater.TestUtils.mockShowing;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.jpmc.theater.TestUtils;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DayTimeRangeBasedTicketDiscountRuleTest {
  @Test
  void discountAmountForShowingBeforeDiscountStartTimeShouldBeCalculatedAsExpected() {
    // given
    var showingDateTime = LocalDateTime.of(2022, 7, 5, 10, 59, 59, 1);
    var ticketPrice = new BigDecimal("15");
    var movie = TestUtils.mockMovie(ticketPrice);
    var showing = mockShowing(movie, showingDateTime);

    // when
    var ticketDiscount = new DayTimeRangeBasedTicketDiscountRule().calculateDiscount(showing);

    // then
    assertNotNull(ticketDiscount);
    assertEquals(new BigDecimal("0.00"), ticketDiscount.getDiscountPercentage());
    assertEquals(new BigDecimal("0.00"), ticketDiscount.getDiscountAmount());
  }

  @Test
  void discountAmountForShowingAfterDiscountStartTimeShouldBeCalculatedAsExpected() {
    // given
    var showingDateTime = LocalDateTime.of(2022, 7, 5, 11, 00, 00, 1);
    var ticketPrice = new BigDecimal("15");
    var movie = TestUtils.mockMovie(ticketPrice);
    var showing = mockShowing(movie, showingDateTime);

    // when
    var ticketDiscount = new DayTimeRangeBasedTicketDiscountRule().calculateDiscount(showing);

    // then
    assertNotNull(ticketDiscount);
    assertEquals(new BigDecimal("25.00"), ticketDiscount.getDiscountPercentage());
    assertEquals(new BigDecimal("3.75"), ticketDiscount.getDiscountAmount());
  }

  @Test
  void discountAmountForShowingBeforDiscountEndTimeShouldBeCalculatedAsExpected() {
    // given
    var showingDateTime = LocalDateTime.of(2022, 7, 5, 15, 59, 59, 1);
    var ticketPrice = new BigDecimal("15");
    var movie = TestUtils.mockMovie(ticketPrice);
    var showing = mockShowing(movie, showingDateTime);

    // when
    var ticketDiscount = new DayTimeRangeBasedTicketDiscountRule().calculateDiscount(showing);

    // then
    assertNotNull(ticketDiscount);
    assertEquals(new BigDecimal("25.00"), ticketDiscount.getDiscountPercentage());
    assertEquals(new BigDecimal("3.75"), ticketDiscount.getDiscountAmount());
  }

  @Test
  void discountAmountForShowingAfterDiscountEdnTimeShouldBeCalculatedAsExpected() {
    // given
    var showingDateTime = LocalDateTime.of(2022, 7, 5, 16, 0, 0, 1);
    var ticketPrice = new BigDecimal("15");
    var movie = TestUtils.mockMovie(ticketPrice);
    var showing = mockShowing(movie, showingDateTime);

    // when
    var ticketDiscount = new DayTimeRangeBasedTicketDiscountRule().calculateDiscount(showing);

    // then
    assertNotNull(ticketDiscount);
    assertEquals(new BigDecimal("0.00"), ticketDiscount.getDiscountPercentage());
    assertEquals(new BigDecimal("0.00"), ticketDiscount.getDiscountAmount());
  }

  @Test
  void negativeTicketPriceDiscountAmountShouldBeCalculatedAsExpected() {
    // given
    var ticketPrice = new BigDecimal("-0.01");
    var movie = TestUtils.mockMovie(ticketPrice);
    var showing = mockShowing(movie, null);

    // when
    var ticketDiscount = new SpecialMovieTicketDiscountRule().calculateDiscount(showing);

    // then
    assertNotNull(ticketDiscount);
    Assertions.assertEquals(TicketDiscount.NO_DISCOUNT, ticketDiscount);
  }
}
