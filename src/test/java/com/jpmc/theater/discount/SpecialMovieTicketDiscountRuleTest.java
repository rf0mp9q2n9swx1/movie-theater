package com.jpmc.theater.discount;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.jpmc.theater.TestUtils;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

class SpecialMovieTicketDiscountRuleTest {
  @Test
  void specialCodeDiscountShouldBeCalculatedAsExpected() {
    // given
    var ticketPrice = new BigDecimal("15");
    var movie = TestUtils.mockMovie(ticketPrice, true);
    var showing = TestUtils.mockShowing(movie);

    // when
    var ticketDiscount = new SpecialMovieTicketDiscountRule().calculateDiscount(showing);

    // then
    assertNotNull(ticketDiscount);
    assertEquals(new BigDecimal("20.00"), ticketDiscount.getDiscountPercentage());
    assertEquals(new BigDecimal("3.00"), ticketDiscount.getDiscountAmount());
  }

  @Test
  void negativeTicketPriceDiscountShouldBeCalculatedAsExpected() {
    // given
    var ticketPrice = new BigDecimal("-0.01");
    var movie = TestUtils.mockMovie(ticketPrice, true);
    var showing = TestUtils.mockShowing(movie);

    // when
    var ticketDiscount = new SpecialMovieTicketDiscountRule().calculateDiscount(showing);

    // then
    assertNotNull(ticketDiscount);
    assertEquals(TicketDiscount.NO_DISCOUNT, ticketDiscount);
  }

  @Test
  void notSpecialCodeDiscountShouldBeCalculatedAsExpected() {
    // given
    var ticketPrice = new BigDecimal("15");
    var movie = TestUtils.mockMovie(ticketPrice, false);
    var showing = TestUtils.mockShowing(movie);

    // when
    var ticketDiscount = new SpecialMovieTicketDiscountRule().calculateDiscount(showing);

    // then
    assertNotNull(ticketDiscount);
    assertEquals(TicketDiscount.NO_DISCOUNT, ticketDiscount);
  }
}
