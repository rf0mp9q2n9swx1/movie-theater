package com.jpmc.theater;

import static java.time.ZoneId.systemDefault;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import org.mockito.Mockito;

public class TestUtils {

  public static Movie mockMovie(BigDecimal ticketPrice) {
    var movie = mock(Movie.class);
    doReturn(ticketPrice).when(movie).getTicketPrice();
    return movie;
  }

  public static Movie mockMovie(BigDecimal ticketPrice, boolean specialCode) {
    var movie = mock(Movie.class);
    doReturn(ticketPrice).when(movie).getTicketPrice();
    doReturn(specialCode).when(movie).isSpecialCode();
    return movie;
  }

  public static Movie mockMovie(Duration duration) {
    var movie = mock(Movie.class);
    doReturn(duration).when(movie).getRunningTime();
    return movie;
  }

  public static Movie mockMovie(String title, BigDecimal ticketPrice, Duration runningTime) {
    var movie = mock(Movie.class);
    doReturn(title).when(movie).getTitle();
    doReturn(ticketPrice).when(movie).getTicketPrice();
    doReturn(runningTime).when(movie).getRunningTime();
    return movie;
  }

  public static Showing mockShowing(Movie movie, LocalDateTime showingStartTime) {
    var showing = mock(Showing.class);
    Mockito.doReturn(movie).when(showing).getMovie();
    Mockito.doReturn(showingStartTime).when(showing).getStartTime();
    return showing;
  }

  public static Showing mockShowing(Movie movie, int sequenceOfTheDay) {
    var showing = mock(Showing.class);
    doReturn(movie).when(showing).getMovie();
    doReturn(sequenceOfTheDay).when(showing).getSequenceOfTheDay();
    return showing;
  }

  public static Showing mockShowing(
      Movie movie, int sequenceOfTheDay, LocalDateTime showingStartTime) {
    var showing = mock(Showing.class);
    doReturn(movie).when(showing).getMovie();
    doReturn(sequenceOfTheDay).when(showing).getSequenceOfTheDay();
    doReturn(showingStartTime).when(showing).getStartTime();
    return showing;
  }

  public static Showing mockShowing(Movie movie) {
    var showing = mock(Showing.class);
    doReturn(movie).when(showing).getMovie();
    return showing;
  }

  public static Clock mockClock(
      int year, int month, int dayOfMonth, int hour, int minute, int second, int nanoOfSecond) {
    var datetime = LocalDateTime.of(year, month, dayOfMonth, hour, minute, second, nanoOfSecond);
    var instant = ZonedDateTime.of(datetime, systemDefault()).toInstant();
    return Clock.fixed(instant, systemDefault());
  }
}
