package com.jpmc.theater;

import static com.jpmc.theater.TestUtils.mockClock;
import static com.jpmc.theater.TestUtils.mockMovie;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.junit.jupiter.api.Test;

class ScheduleTest {
  @Test
  void addingShowingWhithStartTimeOverlappingPreviousShowingShouldThrowException() {
    // given
    var clock = mockClock(2022, 7, 5, 12, 55, 0, 0);
    var localDate = LocalDate.now(clock);

    var schedule = Utils.initSampleSchedule(clock);
    var movie = mockMovie(Duration.ofMinutes(200));

    // when
    var exception =
        assertThrows(
            IllegalStateException.class,
            () -> schedule.addShowing(3, movie, localDate, LocalTime.of(12, 15)));

    // then
    assertNotNull(exception);
    assertEquals(
        "Unable to add showing by 3 sequence of the day "
            + "because its start time overlaps with previous showing's end time.",
        exception.getMessage());
  }

  @Test
  void addingShowingWhithEndTimeOverlappingNextShowingShouldThrowException() {
    // given
    var clock = mockClock(2022, 7, 5, 12, 55, 0, 0);
    var localDate = LocalDate.now(clock);

    var schedule = Utils.initSampleSchedule(clock);
    var movie = mockMovie(Duration.ofMinutes(200));

    // when
    var exception =
        assertThrows(
            IllegalStateException.class,
            () -> schedule.addShowing(1, movie, localDate, LocalTime.of(11, 15)));

    // then
    assertNotNull(exception);
    assertEquals(
        "Unable to add showing  by 1 sequence of the day "
            + "because its end time overlaps with next showing's start time.",
        exception.getMessage());
  }

  @Test
  void addingShowingWhithNoTimeOverlapShouldBeCompletedAsExpected() {
    // given
    var clock = mockClock(2022, 7, 5, 12, 55, 0, 0);
    var localDate = LocalDate.now(clock);

    var schedule = Utils.initSampleSchedule(clock);
    var movie = mockMovie(Duration.ofMinutes(15));

    // when
    schedule.addShowing(1, movie, localDate, LocalTime.of(9, 15));

    // then
    var showing = schedule.getShowing(1);
    assertNotNull(showing);
    assertEquals(1, showing.getSequenceOfTheDay());
    assertEquals(movie, showing.getMovie());
    assertEquals(LocalDateTime.now(clock).withHour(9).withMinute(15), showing.getStartTime());
  }
}
