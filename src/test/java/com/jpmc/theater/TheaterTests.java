package com.jpmc.theater;

import static com.jpmc.theater.TestUtils.mockClock;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

class TheaterTests {

  @Test
  void customerReservationTotalShouldBeCalculatedAsExpected() {
    // given
    var clock = mockClock(2022, 7, 5, 8, 45, 0, 0);
    var theater = new Theater(clock);
    var john = new Customer("John Doe", "id-12345");

    // when
    var reservation = theater.reserve(john, 2, 4);

    // then
    assertNotNull(reservation);
    assertEquals(new BigDecimal("40.00"), reservation.getTotal());
  }
}
