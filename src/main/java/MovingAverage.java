import java.util.Arrays;

public class MovingAverage {
    private final double[] window; // use this data structure
    // you can add other fields here

    /*
     * Always invoked with windowSize > 0
     */
    public MovingAverage(int windowSize) {
        window = new double[1 + windowSize];
    }

    /*
     * Adds the number to window if it is larger than the value of 2.0d.
     * If window is full then replace the oldest added number in the window with the number provided.
     */
    public void addNumberToWindow(double number) {
        if (number <= 2) {
            return;
        }

        var oldest = (int) window[0];
        int idx = oldest + 1;

        if (oldest == window.length - 1) {
            idx = 1;
        }

        window[idx] = number;
        window[0] = idx;
    }

    /*
     * Return the average of the numbers added that are currently present in the window.
     * Otherwise when no numbers in window return 0.0d
     */
    public double getAverage() {
        return Arrays.stream(window).skip(1).filter(d -> d > 0).average().getAsDouble();
    }

    public static void main(String[] args) {
        MovingAverage movingAverage = new MovingAverage(4);
        movingAverage.addNumberToWindow(1.5d);
        movingAverage.addNumberToWindow(2.5d);
        System.out.println(movingAverage.getAverage()); // prints 2.5
        movingAverage.addNumberToWindow(3.0d);
        movingAverage.addNumberToWindow(3.5d);
        System.out.println(movingAverage.getAverage()); // prints 3.0
        movingAverage.addNumberToWindow(2.0d);
        System.out.println(movingAverage.getAverage()); // prints 3.0
        movingAverage.addNumberToWindow(7.0d);
        System.out.println(movingAverage.getAverage()); // prints 4.0
        movingAverage.addNumberToWindow(6.5d);
        System.out.println(movingAverage.getAverage()); // prints 5.0
    }
}