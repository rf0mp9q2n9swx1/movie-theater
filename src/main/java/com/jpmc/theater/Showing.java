package com.jpmc.theater;

import java.time.LocalDateTime;
import java.util.Objects;

public class Showing {
  private final Movie movie;
  private final int sequenceOfTheDay;
  private final LocalDateTime startTime;

  public Showing(Movie movie, int sequenceOfTheDay, LocalDateTime startTime) {
    this.movie = movie;
    this.sequenceOfTheDay = sequenceOfTheDay;
    this.startTime = startTime;
  }

  public Movie getMovie() {
    return movie;
  }

  public int getSequenceOfTheDay() {
    return sequenceOfTheDay;
  }

  public LocalDateTime getStartTime() {
    return startTime;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Showing)) {
      return false;
    }
    Showing showing = (Showing) o;
    return sequenceOfTheDay == showing.sequenceOfTheDay
        && Objects.equals(movie, showing.movie)
        && Objects.equals(startTime, showing.startTime);
  }

  @Override
  public int hashCode() {
    return Objects.hash(movie, sequenceOfTheDay, startTime);
  }

  @Override
  public String toString() {
    return "Showing{"
        + "movie="
        + movie
        + ", sequenceOfTheDay="
        + sequenceOfTheDay
        + ", startTime="
        + startTime
        + '}';
  }
}
