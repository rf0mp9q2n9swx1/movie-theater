package com.jpmc.theater;

import static com.jpmc.theater.Utils.initSampleSchedule;

import com.jpmc.theater.discount.TicketDiscountCalculator;
import com.jpmc.theater.printer.SchedulePrinter;
import com.jpmc.theater.printer.SchedulePrinter.Mode;
import java.time.Clock;

public class Theater {
  private final Schedule schedule;

  public Theater(Clock clock) {
    this.schedule = initSampleSchedule(clock);
  }

  public Reservation reserve(Customer customer, int sequence, int howManyTickets) {
    var showing = schedule.getShowing(sequence);
    var ticketDiscount = TicketDiscountCalculator.INSTANCE.apply(showing);
    return new Reservation(customer, showing, ticketDiscount, howManyTickets);
  }

  public void printSchedule(Mode mode) {
    var printer = SchedulePrinter.getPrinter(mode);
    printer.print(System.out, this.schedule);
  }

  public static void main(String[] args) {
    var clock = Clock.systemDefaultZone();

    Theater theater = new Theater(clock);
    theater.printSchedule(Mode.PLAIN_TEXT);
  }
}
