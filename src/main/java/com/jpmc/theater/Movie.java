package com.jpmc.theater;

import static com.jpmc.theater.Utils.scale;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Objects;

public class Movie {
  private final String title;
  private final Duration runningTime;
  private final BigDecimal ticketPrice;
  private final boolean specialCode;

  public Movie(String title, Duration runningTime, double ticketPrice, boolean specialCode) {
    this.title = title;
    this.runningTime = runningTime;
    this.ticketPrice = scale(ticketPrice);
    this.specialCode = specialCode;
  }

  public Movie(String title, Duration runningTime, double ticketPrice) {
    this(title, runningTime, ticketPrice, false);
  }

  public String getTitle() {
    return title;
  }

  public Duration getRunningTime() {
    return runningTime;
  }

  public BigDecimal getTicketPrice() {
    return ticketPrice;
  }

  public boolean isSpecialCode() {
    return specialCode;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Movie)) {
      return false;
    }
    Movie movie = (Movie) o;
    return specialCode == movie.specialCode
        && Objects.equals(title, movie.title)
        && Objects.equals(runningTime, movie.runningTime)
        && Objects.equals(ticketPrice, movie.ticketPrice);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title, runningTime, ticketPrice, specialCode);
  }

  @Override
  public String toString() {
    return "Movie{"
        + "title='"
        + title
        + '\''
        + ", runningTime="
        + runningTime
        + ", ticketPrice="
        + ticketPrice
        + ", specialCode="
        + specialCode
        + '}';
  }
}
