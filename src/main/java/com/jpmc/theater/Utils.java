package com.jpmc.theater;

import static java.math.RoundingMode.HALF_UP;
import static java.time.Duration.ofMinutes;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalTime;

public class Utils {

  private Utils() {
    throw new IllegalStateException("Utility class");
  }

  public static final int PRICE_SCALE = 2;
  public static final RoundingMode PRICE_ROUNDING_MODE = HALF_UP;

  public static BigDecimal scale(BigDecimal value) {
    return value.setScale(PRICE_SCALE, PRICE_ROUNDING_MODE);
  }

  public static BigDecimal scale(double value) {
    return scale(BigDecimal.valueOf(value));
  }

  public static Schedule initSampleSchedule(Clock clock) {
    var schedule = new Schedule(clock);
    var currentDate = LocalDate.now(clock);

    var spiderMan = new Movie("Spider-Man: No Way Home", ofMinutes(90), 12.5, true);
    var turningRed = new Movie("Turning Red", ofMinutes(85), 11);
    var theBatMan = new Movie("The Batman", ofMinutes(95), 9);

    return schedule
        .addShowing(1, turningRed, currentDate, LocalTime.of(9, 0))
        .addShowing(2, spiderMan, currentDate, LocalTime.of(11, 0))
        .addShowing(3, theBatMan, currentDate, LocalTime.of(12, 50))
        .addShowing(4, turningRed, currentDate, LocalTime.of(14, 30))
        .addShowing(5, spiderMan, currentDate, LocalTime.of(16, 10))
        .addShowing(6, theBatMan, currentDate, LocalTime.of(17, 50))
        .addShowing(7, turningRed, currentDate, LocalTime.of(19, 30))
        .addShowing(8, spiderMan, currentDate, LocalTime.of(21, 10))
        .addShowing(9, theBatMan, currentDate, LocalTime.of(23, 0));
  }
}
