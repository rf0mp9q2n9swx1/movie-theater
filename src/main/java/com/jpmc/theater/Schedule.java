package com.jpmc.theater;

import static java.util.Objects.isNull;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Schedule {
  private final Clock clock;
  private final Map<Integer, Showing> showingsBySequenceOfTheDay = new HashMap<>();

  public Schedule(Clock clock) {
    this.clock = clock;
  }

  public Clock getClock() {
    return clock;
  }

  public Collection<Showing> getShowings() {
    return showingsBySequenceOfTheDay.values();
  }

  public Schedule addShowing(int sequenceOfTheDay, Movie movie, LocalDate date, LocalTime time) {
    var startTime = LocalDateTime.of(date, time);
    var showing = new Showing(movie, sequenceOfTheDay, startTime);
    validateTimeOverlaps(showing);

    this.showingsBySequenceOfTheDay.put(sequenceOfTheDay, showing);
    return this;
  }

  public Showing getShowing(int sequenceOfTheDay) {
    var showing = showingsBySequenceOfTheDay.get(sequenceOfTheDay);

    if (isNull(showing)) {
      throw new IllegalStateException(
          "Unable to find showing by " + sequenceOfTheDay + " sequence of the day.");
    }

    return showing;
  }

  private void validateTimeOverlaps(Showing showing) {
    if (showingsBySequenceOfTheDay.isEmpty()) {
      return;
    }

    var sequenceOfTheDay = showing.getSequenceOfTheDay();

    // validate start time overlap with the previous showing
    var prevShowing = showingsBySequenceOfTheDay.get(sequenceOfTheDay - 1);
    validateTimeOverlapWithPrevShowing(showing, prevShowing);

    // validate start time overlap with the next showing
    var nextShowing = showingsBySequenceOfTheDay.get(sequenceOfTheDay + 1);
    validateTimeOverlapWithNextShowing(showing, nextShowing);
  }

  private void validateTimeOverlapWithNextShowing(Showing showing, Showing nextShowing) {
    if (isNull(nextShowing)) {
      return;
    }

    var showingStartTime = showing.getStartTime();
    var showingDuration = showing.getMovie().getRunningTime();
    var showingEndTime = showingStartTime.plus(showingDuration);
    var nextStartTime = nextShowing.getStartTime();

    if (showingEndTime.isAfter(nextStartTime)) {
      var message =
          "Unable to add showing  by "
              + showing.getSequenceOfTheDay()
              + " sequence of the day because its end time "
              + "overlaps with next showing's start time.";
      throw new IllegalStateException(message);
    }
  }

  private void validateTimeOverlapWithPrevShowing(Showing showing, Showing prevShowing) {
    if (isNull(prevShowing)) {
      return;
    }

    var showingStartTime = showing.getStartTime();
    var prevStartTime = prevShowing.getStartTime();
    var prevDuration = prevShowing.getMovie().getRunningTime();
    var prevEndTime = prevStartTime.plus(prevDuration);

    if (showingStartTime.isBefore(prevEndTime)) {
      var message =
          "Unable to add showing by "
              + showing.getSequenceOfTheDay()
              + " sequence of the day because its start time "
              + "overlaps with previous showing's end time.";
      throw new IllegalStateException(message);
    }
  }
}
