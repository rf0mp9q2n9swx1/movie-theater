package com.jpmc.theater;

import static com.jpmc.theater.Utils.scale;

import com.jpmc.theater.discount.TicketDiscount;
import java.math.BigDecimal;
import java.util.Objects;

public class Reservation {
  private final Customer customer;
  private final Showing showing;
  private final BigDecimal ticketPrice;
  private final TicketDiscount ticketDiscount;
  private final int audienceCount;
  private final BigDecimal ticketPriceTotal;
  private final TicketDiscount ticketDiscountTotal;
  private final BigDecimal total;

  public Reservation(
      Customer customer, Showing showing, TicketDiscount ticketDiscount, int audienceCount) {
    this.customer = customer;
    this.showing = showing;
    this.ticketPrice = scale(showing.getMovie().getTicketPrice());
    this.ticketDiscount = ticketDiscount;
    this.audienceCount = audienceCount;
    this.ticketPriceTotal = calculateTicketPriceTotal(audienceCount, ticketPrice);
    this.ticketDiscountTotal = calculateTicketDiscountTotal(audienceCount, ticketDiscount);
    this.total = calculateTotal(this.ticketPriceTotal, this.ticketDiscountTotal);
  }

  public Customer getCustomer() {
    return customer;
  }

  public Showing getShowing() {
    return showing;
  }

  public BigDecimal getTicketPrice() {
    return ticketPrice;
  }

  public TicketDiscount getTicketDiscount() {
    return ticketDiscount;
  }

  public int getAudienceCount() {
    return audienceCount;
  }

  public BigDecimal getTicketPriceTotal() {
    return ticketPriceTotal;
  }

  public TicketDiscount getTicketDiscountTotal() {
    return ticketDiscountTotal;
  }

  public BigDecimal getTotal() {
    return total;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Reservation)) {
      return false;
    }
    Reservation that = (Reservation) o;
    return audienceCount == that.audienceCount
        && Objects.equals(customer, that.customer)
        && Objects.equals(showing, that.showing)
        && Objects.equals(ticketPrice, that.ticketPrice)
        && Objects.equals(ticketDiscount, that.ticketDiscount)
        && Objects.equals(ticketPriceTotal, that.ticketPriceTotal)
        && Objects.equals(ticketDiscountTotal, that.ticketDiscountTotal)
        && Objects.equals(total, that.total);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        customer,
        showing,
        ticketPrice,
        ticketDiscount,
        audienceCount,
        ticketPriceTotal,
        ticketDiscountTotal,
        total);
  }

  @Override
  public String toString() {
    return "Reservation{"
        + "customer="
        + customer
        + ", showing="
        + showing
        + ", ticketPrice="
        + ticketPrice
        + ", ticketDiscount="
        + ticketDiscount
        + ", audienceCount="
        + audienceCount
        + ", ticketPriceTotal="
        + ticketPriceTotal
        + ", ticketDiscountTotal="
        + ticketDiscountTotal
        + ", total="
        + total
        + '}';
  }

  private BigDecimal calculateTotal(
      BigDecimal ticketPriceTotal, TicketDiscount ticketDiscountTotal) {
    var ticketTotalDiscountAmount = ticketDiscountTotal.getDiscountAmount();
    var calculatedTotal = ticketPriceTotal.subtract(ticketTotalDiscountAmount);
    return scale(calculatedTotal);
  }

  private TicketDiscount calculateTicketDiscountTotal(
      int audienceCount, TicketDiscount ticketDiscount) {
    var totalDiscountAmount =
        ticketDiscount.getDiscountAmount().multiply(BigDecimal.valueOf(audienceCount));
    var discountPercentage = ticketDiscount.getDiscountPercentage();
    return new TicketDiscount(totalDiscountAmount, discountPercentage);
  }

  private BigDecimal calculateTicketPriceTotal(int audienceCount, BigDecimal ticketPrice) {
    var calculatedTicketPriceTotal = ticketPrice.multiply(BigDecimal.valueOf(audienceCount));
    return scale(calculatedTicketPriceTotal);
  }
}
