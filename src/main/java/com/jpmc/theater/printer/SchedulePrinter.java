package com.jpmc.theater.printer;

import static java.util.concurrent.TimeUnit.HOURS;

import com.jpmc.theater.Schedule;
import java.io.OutputStream;
import java.time.Duration;

public interface SchedulePrinter {
  String NEW_LINE = System.getProperty("line.separator");

  static SchedulePrinter getPrinter(Mode mode) {
    if (Mode.JSON.equals(mode)) {
      return new JsonSchedulePrinter();
    } else {
      return new PlainTextSchedulePrinter();
    }
  }

  void print(OutputStream outputStream, Schedule schedule);

  static String humanReadableFormat(Duration duration) {
    long hour = duration.toHours();
    long remainingMin = duration.toMinutes() - HOURS.toMinutes(duration.toHours());

    return String.format(
        "(%s hour%s %s minute%s)",
        hour, handlePlural(hour), remainingMin, handlePlural(remainingMin));
  }

  // (s) postfix should be added to handle plural correctly
  private static String handlePlural(long value) {
    if (value == 1) {
      return "";
    } else {
      return "s";
    }
  }

  enum Mode {
    PLAIN_TEXT,
    JSON
  }
}
