package com.jpmc.theater.printer;

import static java.nio.charset.StandardCharsets.UTF_8;

import com.google.gson.stream.JsonWriter;
import com.jpmc.theater.Schedule;
import com.jpmc.theater.Showing;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.time.Clock;
import java.time.LocalDate;

public class JsonSchedulePrinter implements SchedulePrinter {

  @Override
  public void print(OutputStream outputStream, Schedule schedule) {
    try (Writer writer = new OutputStreamWriter(outputStream, UTF_8);
        JsonWriter jsonWriter = new JsonWriter(writer)) {
      var clock = schedule.getClock();

      jsonWriter.beginObject();
      printCurrenDate(jsonWriter, clock);
      printShowings(jsonWriter, schedule);
      jsonWriter.endObject();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void printShowings(JsonWriter jsonWriter, Schedule schedule) throws IOException {
    jsonWriter.name("showings");
    jsonWriter.beginArray();

    for (Showing showing : schedule.getShowings()) {
      printShowing(jsonWriter, showing);
    }

    jsonWriter.endArray();
  }

  private void printShowing(JsonWriter jsonWriter, Showing showing) throws IOException {
    var runningTime = SchedulePrinter.humanReadableFormat(showing.getMovie().getRunningTime());
    var movieFee = "$" + showing.getMovie().getTicketPrice();

    jsonWriter.beginObject();
    jsonWriter.name("sequenceOfTheDay").value(showing.getSequenceOfTheDay());
    jsonWriter.name("startTime").value(showing.getStartTime().toString());
    jsonWriter.name("title").value(showing.getMovie().getTitle());
    jsonWriter.name("runningTime").value(runningTime);
    jsonWriter.name("movieFee").value(movieFee);
    jsonWriter.endObject();
  }

  private void printCurrenDate(JsonWriter jsonWriter, Clock clock) throws IOException {
    var currentDate = LocalDate.now(clock);
    jsonWriter.name("date").value(currentDate.toString());
  }
}
