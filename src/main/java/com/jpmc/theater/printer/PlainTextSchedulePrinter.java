package com.jpmc.theater.printer;

import static java.nio.charset.StandardCharsets.UTF_8;

import com.jpmc.theater.Schedule;
import com.jpmc.theater.Showing;
import java.io.OutputStream;
import java.time.Clock;
import java.time.LocalDate;

public class PlainTextSchedulePrinter implements SchedulePrinter {
  @Override
  public void print(OutputStream outputStream, Schedule schedule) {
    var clock = schedule.getClock();

    printCurrenDate(outputStream, clock);
    printLine(outputStream);
    printShowings(outputStream, schedule);
    printLine(outputStream);
  }

  private void printShowings(OutputStream outputStream, Schedule schedule) {
    for (Showing showing : schedule.getShowings()) {
      printShowing(outputStream, showing);
    }
  }

  private void printShowing(OutputStream outputStream, Showing showing) {
    var sequenceOfTheDay = showing.getSequenceOfTheDay();
    var startTime = showing.getStartTime();
    var title = showing.getMovie().getTitle();
    var runningTime = SchedulePrinter.humanReadableFormat(showing.getMovie().getRunningTime());
    var movieFee = "$" + showing.getMovie().getTicketPrice();

    var plainText =
        String.format("%d: %s %s %s %s", sequenceOfTheDay, startTime, title, runningTime, movieFee);
    writeLine(outputStream, plainText);
  }

  private void printLine(OutputStream outputStream) {
    var line = "===================================================";
    writeLine(outputStream, line);
  }

  private void printCurrenDate(OutputStream outputStream, Clock clock) {
    var currentDate = LocalDate.now(clock);
    writeLine(outputStream, currentDate.toString());
  }

  private void writeLine(OutputStream outputStream, String line) {
    try {
      outputStream.write(line.getBytes(UTF_8));
      outputStream.write(NEW_LINE.getBytes(UTF_8));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
