package com.jpmc.theater.discount;

import static com.jpmc.theater.Utils.scale;

import java.math.BigDecimal;
import java.util.Objects;

public class TicketDiscount {
  public static final TicketDiscount NO_DISCOUNT =
      new TicketDiscount(BigDecimal.ZERO, BigDecimal.ZERO);

  private final BigDecimal discountAmount;
  private final BigDecimal discountPercentage;

  public TicketDiscount(BigDecimal discountAmount, BigDecimal discountPercentage) {
    this.discountAmount = scale(discountAmount);
    this.discountPercentage = scale(discountPercentage);
  }

  public BigDecimal getDiscountAmount() {
    return discountAmount;
  }

  public BigDecimal getDiscountPercentage() {
    return discountPercentage;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof TicketDiscount)) {
      return false;
    }
    TicketDiscount that = (TicketDiscount) o;
    return Objects.equals(discountAmount, that.discountAmount)
        && Objects.equals(discountPercentage, that.discountPercentage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(discountAmount, discountPercentage);
  }

  @Override
  public String toString() {
    return "TicketDiscount{"
        + "discountAmount="
        + discountAmount
        + ", discountPercentage="
        + discountPercentage
        + '}';
  }
}
