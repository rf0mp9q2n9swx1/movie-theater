package com.jpmc.theater.discount;

import static java.util.Collections.unmodifiableCollection;
import static java.util.stream.Collectors.toList;

import com.jpmc.theater.Showing;
import java.util.Collection;
import java.util.LinkedList;
import java.util.function.Function;

/**
 * Implements "The discount amount applied only one if met multiple rules; biggest amount one" rule
 */
public enum TicketDiscountCalculator implements Function<Showing, TicketDiscount> {
  INSTANCE;

  private static final Collection<TicketDiscountRule> DISCOUNT_RULES = allDiscountRules();

  @Override
  public TicketDiscount apply(Showing showing) {
    var discounts =
        DISCOUNT_RULES.stream()
            .map(rule -> rule.calculateDiscount(showing))
            .sorted(TicketDiscountComparator.INSTANCE)
            .collect(toList());

    return discounts.isEmpty() ? TicketDiscount.NO_DISCOUNT : discounts.get(0);
  }

  private static Collection<TicketDiscountRule> allDiscountRules() {
    var allDiscountRules = new LinkedList<TicketDiscountRule>();
    allDiscountRules.add(new SpecialMovieTicketDiscountRule());
    allDiscountRules.add(new FirstShowingTicketDiscountRule());
    allDiscountRules.add(new SecondShowingTicketDiscountRule());
    allDiscountRules.add(new DayTimeRangeBasedTicketDiscountRule());
    allDiscountRules.add(new MonthDayBasedTicketDiscountRule());

    return unmodifiableCollection(allDiscountRules);
  }
}
