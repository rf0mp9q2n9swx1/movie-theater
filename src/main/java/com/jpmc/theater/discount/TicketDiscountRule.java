package com.jpmc.theater.discount;

import static java.util.Optional.ofNullable;

import com.jpmc.theater.Movie;
import com.jpmc.theater.Showing;
import java.math.BigDecimal;

public interface TicketDiscountRule {
  BigDecimal ONE_HUNDRED = new BigDecimal(100);

  boolean isQualifiedForDiscount(Showing showing);

  TicketDiscount calculateDiscount(Showing showing);

  default boolean isTicketPriceLessOrEqualsToZero(Showing showing) {
    return ofNullable(showing)
        .map(Showing::getMovie)
        .map(Movie::getTicketPrice)
        .map(p -> p.compareTo(BigDecimal.ZERO) < 0)
        .orElse(false);
  }
}
