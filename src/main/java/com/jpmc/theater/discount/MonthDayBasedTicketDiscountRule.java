package com.jpmc.theater.discount;

import com.jpmc.theater.Showing;
import java.math.BigDecimal;

/** Implements "Any movies showing on 7th, you'll get 1$ discount" rule */
public class MonthDayBasedTicketDiscountRule extends AmountBasedTicketDiscountRule {
  private static final BigDecimal DISCOUNT_AMOUNT = new BigDecimal(1);
  private static final int DISCOUNT_DAY_OF_MONTH = 7;

  @Override
  protected BigDecimal getDiscountAmount() {
    return DISCOUNT_AMOUNT;
  }

  @Override
  public boolean isQualifiedForDiscount(Showing showing) {
    if (isTicketPriceLessOrEqualsToZero(showing)) {
      return false;
    }

    var startTime = showing.getStartTime();
    return startTime.getDayOfMonth() == DISCOUNT_DAY_OF_MONTH;
  }
}
