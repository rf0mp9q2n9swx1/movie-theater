package com.jpmc.theater.discount;

import com.jpmc.theater.Showing;
import java.math.BigDecimal;

/** Implements "$3 discount for the movie showing 1st of the day" rule */
public class FirstShowingTicketDiscountRule extends AmountBasedTicketDiscountRule {
  private static final BigDecimal DISCOUNT_AMOUNT = new BigDecimal(3);
  private static final int DISCOUNT_DAY_SEQ = 1;

  @Override
  protected BigDecimal getDiscountAmount() {
    return DISCOUNT_AMOUNT;
  }

  @Override
  public boolean isQualifiedForDiscount(Showing showing) {
    if (isTicketPriceLessOrEqualsToZero(showing)) {
      return false;
    }

    return showing.getSequenceOfTheDay() == DISCOUNT_DAY_SEQ;
  }
}
