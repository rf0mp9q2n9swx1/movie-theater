package com.jpmc.theater.discount;

import static com.jpmc.theater.Utils.PRICE_ROUNDING_MODE;
import static com.jpmc.theater.Utils.PRICE_SCALE;
import static java.util.Objects.isNull;

import com.jpmc.theater.Showing;
import java.math.BigDecimal;

public abstract class AmountBasedTicketDiscountRule implements TicketDiscountRule {
  protected abstract BigDecimal getDiscountAmount();

  @Override
  public TicketDiscount calculateDiscount(Showing showing) {
    boolean qualified = isQualifiedForDiscount(showing);

    if (!qualified) {
      return TicketDiscount.NO_DISCOUNT;
    }

    var movie = showing.getMovie();

    if (isNull(movie)) {
      return TicketDiscount.NO_DISCOUNT;
    }

    var ticketPrice = movie.getTicketPrice();
    var discountAmount = getDiscountAmount();
    var delta = ticketPrice.subtract(discountAmount);
    var maxDiscountAmount = delta.compareTo(BigDecimal.ZERO) >= 0 ? discountAmount : ticketPrice;
    var discountPercentage =
        maxDiscountAmount
            .multiply(ONE_HUNDRED)
            .divide(ticketPrice, PRICE_SCALE, PRICE_ROUNDING_MODE);

    return new TicketDiscount(maxDiscountAmount, discountPercentage);
  }
}
