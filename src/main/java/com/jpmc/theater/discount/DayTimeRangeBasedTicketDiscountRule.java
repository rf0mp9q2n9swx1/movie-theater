package com.jpmc.theater.discount;

import static java.util.Optional.ofNullable;

import com.jpmc.theater.Showing;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/** Implements "Any movies showing starting between 11AM ~ 4pm, you'll get 25% discount" rule */
public class DayTimeRangeBasedTicketDiscountRule extends PercentageBasedTicketDiscountRule {
  private static final BigDecimal DISCOUNT_PERCENTAGE = new BigDecimal(25);
  private static final int HOUR_OF_DAY_STARTS = 11;
  private static final int HOUR_OF_DAY_ENDS = 16;

  @Override
  protected BigDecimal getDiscountPercentage() {
    return DISCOUNT_PERCENTAGE;
  }

  @Override
  public boolean isQualifiedForDiscount(Showing showing) {
    if (isTicketPriceLessOrEqualsToZero(showing)) {
      return false;
    }

    return ofNullable(showing).map(this::isWithInTheRange).orElse(false);
  }

  private boolean isWithInTheRange(Showing showing) {
    var startTime = showing.getStartTime();
    var discountStartsAt =
        LocalDateTime.from(startTime)
            .withHour(HOUR_OF_DAY_STARTS)
            .withMinute(0)
            .withSecond(0)
            .withNano(0);
    var discountEndsAt =
        LocalDateTime.from(startTime)
            .withHour(HOUR_OF_DAY_ENDS)
            .withMinute(0)
            .withSecond(0)
            .withNano(0);

    return startTime.isAfter(discountStartsAt) && startTime.isBefore(discountEndsAt);
  }
}
