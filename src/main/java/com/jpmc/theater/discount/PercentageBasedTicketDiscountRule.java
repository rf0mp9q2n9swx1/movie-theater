package com.jpmc.theater.discount;

import static com.jpmc.theater.Utils.PRICE_ROUNDING_MODE;
import static com.jpmc.theater.Utils.PRICE_SCALE;
import static com.jpmc.theater.Utils.scale;
import static java.util.Objects.isNull;

import com.jpmc.theater.Showing;
import java.math.BigDecimal;

public abstract class PercentageBasedTicketDiscountRule implements TicketDiscountRule {

  protected abstract BigDecimal getDiscountPercentage();

  @Override
  public TicketDiscount calculateDiscount(Showing showing) {
    boolean qualified = isQualifiedForDiscount(showing);

    if (!qualified) {
      return TicketDiscount.NO_DISCOUNT;
    }

    var movie = showing.getMovie();

    if (isNull(movie)) {
      return TicketDiscount.NO_DISCOUNT;
    }

    var ticketPrice = movie.getTicketPrice();
    var discountPercentage = getDiscountPercentage();
    var factor = discountPercentage.divide(ONE_HUNDRED, PRICE_SCALE, PRICE_ROUNDING_MODE);
    var discount = scale(ticketPrice).multiply(factor);

    return new TicketDiscount(discount, discountPercentage);
  }
}
