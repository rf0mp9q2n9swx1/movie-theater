package com.jpmc.theater.discount;

import com.jpmc.theater.Movie;
import com.jpmc.theater.Showing;
import java.math.BigDecimal;
import java.util.Optional;

/** Implements "20% discount for the special movie" rule */
public class SpecialMovieTicketDiscountRule extends PercentageBasedTicketDiscountRule {
  private static final BigDecimal DISCOUNT_PERCENTAGE = new BigDecimal(20);

  @Override
  protected BigDecimal getDiscountPercentage() {
    return DISCOUNT_PERCENTAGE;
  }

  @Override
  public boolean isQualifiedForDiscount(Showing showing) {
    if (isTicketPriceLessOrEqualsToZero(showing)) {
      return false;
    }

    return Optional.ofNullable(showing)
        .map(Showing::getMovie)
        .map(Movie::isSpecialCode)
        .orElse(false);
  }
}
