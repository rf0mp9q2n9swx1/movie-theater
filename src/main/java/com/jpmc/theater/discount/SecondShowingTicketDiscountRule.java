package com.jpmc.theater.discount;

import static java.util.Optional.ofNullable;

import com.jpmc.theater.Showing;
import java.math.BigDecimal;

/** Implements "$2 discount for the movie showing 2nd of the day" rule */
public class SecondShowingTicketDiscountRule extends AmountBasedTicketDiscountRule {
  private static final BigDecimal DISCOUNT_AMOUNT = new BigDecimal(2);
  private static final int SHOWING_SEQUENCE = 2;

  @Override
  protected BigDecimal getDiscountAmount() {
    return DISCOUNT_AMOUNT;
  }

  @Override
  public boolean isQualifiedForDiscount(Showing showing) {
    if (isTicketPriceLessOrEqualsToZero(showing)) {
      return false;
    }

    return ofNullable(showing).map(s -> s.getSequenceOfTheDay() == SHOWING_SEQUENCE).orElse(false);
  }
}
