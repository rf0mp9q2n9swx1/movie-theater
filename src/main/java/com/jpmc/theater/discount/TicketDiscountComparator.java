package com.jpmc.theater.discount;

import java.util.Comparator;

enum TicketDiscountComparator implements Comparator<TicketDiscount> {
  INSTANCE;

  @Override
  public int compare(TicketDiscount t1, TicketDiscount t2) {
    var amount1 = t1.getDiscountAmount();
    var amount2 = t2.getDiscountAmount();
    return amount2.compareTo(amount1);
  }
}
